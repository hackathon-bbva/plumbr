FROM rocker/r-ver:3.6.3

LABEL maintainer "Fabian Santander <hdmsantander@gmail.com>"

ENV CHROME_BIN=/usr/bin/google-chrome

RUN apt-get update && apt-get install -y \
    sudo \
    curl \
    wget \
    libcurl4-openssl-dev \
    libssl-dev \
    libxml2-dev \
    libxt-dev \
    libgit2-dev \
    libjpeg-dev \
    libpq-dev \
    libsodium-dev \
    libudunits2-dev \
    zlib1g-dev \
    wget
    
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

RUN R -e "install.packages(c('xml2', 'rvest', 'DBI', 'plumber', 'RPostgres', 'RPostgreSQL', 'promises', 'rvest', 'httr', 'jsonlite', 'reshape2', 'readxl', 'remotes'), repos='https://cloud.r-project.org/', dependencies=TRUE)"

RUN R -e "remotes::install_github('rlesur/crrri')"

RUN apt-get install -y \
    python3 \
    python3-pip \
    build-essential \
    libffi-dev \
    python-dev

RUN pip3 install selenium bs4 flask

RUN pip3 install chromedriver_installer \
    --install-option="--chromedriver-version=2.10" \
    --install-option="--chromedriver-checksums=4fecc99b066cb1a346035bf022607104,058cd8b7b4b9688507701b5e648fd821"

CMD ["/usr/local/bin/R"]
